/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-cam-take-photo-save-microsd-card
  
  IMPORTANT!!! 
   - Select Board "ESP32 Wrover Module"
   - Select the Partion Scheme "Huge APP (3MB No OTA)
   - GPIO 0 must be connected to GND to upload a sketch
   - After connecting GPIO 0 to GND, press the ESP32-CAM on-board RESET button to put your board in flashing mode
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

#include "esp_camera.h"
#include "esp_timer.h"
#include "img_converters.h"
#include "Arduino.h"
#include "fb_gfx.h"
#include "fd_forward.h"
#include "fr_forward.h"
#include "FS.h"                // SD Card ESP32
#include "SD_MMC.h"            // SD Card ESP32
#include "soc/soc.h"           // Disable brownour problems
#include "soc/rtc_cntl_reg.h"  // Disable brownour problems
//#include "dl_lib.h"
#include "driver/rtc_io.h"
#include <EEPROM.h>            // read and write from flash memory


// define the number of bytes you want to access
#define EEPROM_SIZE 1

// Pin definition for CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

int pictureNumber = 0;
  String path="";

  int capture_interval = 180000; //un asco pero temporalmente va esto (en realidad, debe sacar y ponerse a dormir)

long current_millis;
long last_capture_millis = 0;

  camera_config_t config;
void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); 
 
  Serial.begin(9600);
 /* if(!SPIFFS.begin(true)){
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
}
SPIFFS.format(); //No debería, pero empiricamente es necesario.
*/


 //Serial.println("Starting SD Card");
  if(!SD_MMC.begin()){
  //  Serial.println("SD Card Mount Failed");
    return;
  }
  
  uint8_t cardType = SD_MMC.cardType();
  if(cardType == CARD_NONE){
   // Serial.println("No SD Card attached");
    return;
  }
  
  //Serial.setDebugOutput(true);
  //Serial.println();
  
    rtc_gpio_hold_dis(GPIO_NUM_4);
    config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG; 
  
  if(psramFound()){
    config.frame_size = FRAMESIZE_QVGA; // FRAMESIZE_ + QVGA|CIF|VGA|SVGA|XGA|SXGA|UXGA
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_QVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  
   // Init Camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }
  
}

void loop() {
 current_millis = millis();
  if (current_millis - last_capture_millis > capture_interval) { // Take another picture
    last_capture_millis = millis();
    sacar_foto();
  }

}

void sacar_foto(){

    
   camera_fb_t *fb = esp_camera_fb_get();  //aca saco la foto y la guardo en un buffer
  if(!fb) {
 //   Serial.println("Camera capture failed");
    return;
  }





    size_t _jpg_buf_len = 0;
    uint8_t * _jpg_buf = NULL;

    dl_matrix3du_t *image_matrix = dl_matrix3du_alloc(1, fb->width, fb->height, 3); //aca defino un puntero(? image_matrix

    fmt2rgb888(fb->buf, fb->len, fb->format, image_matrix->item); //aca convierto el coso en rgb888, pero no me queda claro si opera sobre image_matrix o genera un nuevo coso llamado item

// HERE print some text
rgb_print(image_matrix, 0x000000FF, "Texto de prueba");
  dl_matrix3du_free(image_matrix);
bool jpeg_converted = fmt2jpg(image_matrix->item, fb->width*fb->height*3, fb->width, fb->height, PIXFORMAT_RGB888, 90, &_jpg_buf, &_jpg_buf_len);

//entiendo que con jpeg_converted aplica todo lo hecho en image_matrix al buffer original fb...

delay(300); //porque segun lo que dicen, tarda eso en esta resolucion en printear en la imagen y guardar a jpeg.


  

  



  // Path where new picture will be saved in SD Card
path = "/pic.jpg";

 //SPIFFS.remove(path); 
  
  fs::FS &fs = SD_MMC; 

  File file = fs.open(path.c_str(), FILE_WRITE);
  if(!file){
    //jodete
  } 
  else {
    file.write(fb->buf, fb->len); // payload (image), payload length

  }
  file.close();
   Serial.flush();     

  delay(300);

  
    File photoFile = SD_MMC.open(path.c_str()); 

        if (photoFile) { 
          while (photoFile.position() < photoFile.size()) { 

          
                      
            Serial.write(photoFile.read());  //aca empezamos a mandarlo por serie.
            delay(10);
          } 

          photoFile.close(); 
            
        }   
Serial.print("GPS34S58W"); //placeholder


//GPS LATLONGALT
  Serial.print("ok");

  esp_camera_fb_return(fb); //liberen al buffer!


  }


//este choclo se supone hace la magia:
    static void rgb_print(dl_matrix3du_t *image_matrix, uint32_t color, const char * str){
               fb_data_t fb;
               fb.width = image_matrix->w;
               fb.height = image_matrix->h;
               fb.data = image_matrix->item;
               fb.bytes_per_pixel = 3;
               fb.format = FB_BGR888;
               fb_gfx_print(&fb, (fb.width - (strlen(str) * 14)) / 2, 10, color, str);
}